﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Desktop_Exercise_5.Interfaces;
using Desktop_Exercise_5.Utility;

namespace Desktop_Exercise_5.Models
{
  public class Sport : ISport
  {
    private string _sportName;

    private Lookups.SportCategory _category;

    public Sport(string name, int numPlayers, Lookups.SportCategory category)
    {
      _sportName = name;
      NumberOfPlayers = numPlayers;
      _category = category;
    }

    public Utility.Lookups.SportCategory SportCategory
    {
      get { return _category; }
    }

    public int NumberOfPlayers { get; set; }

      public string SportName
    {
      get { return _sportName; }
    }

    public void WriteSportProperties()
    {
      Console.WriteLine("Sport name: {0}\r\n Number of Players: {1}\r\n Category: {2}\r\n Length: {3}\r\n Width: {4} \r\n",_sportName, NumberOfPlayers, _category, Length, Width);
    }

    public decimal Length
    { get; set; }

    public decimal Width
    { get; set; }
  }
}
