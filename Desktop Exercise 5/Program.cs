﻿using System;
using System.Collections.Generic;
using Desktop_Exercise_5.Models;
using Desktop_Exercise_5.Utility;

namespace Desktop_Exercise_5
{
  class Program
  {
    static void Main(string[] args)
    {
      var sport1 = new Sport ("Ballet", 8, Lookups.SportCategory.Single);
      var sport2 = new Sport("Gator Wras'lin", 2, Lookups.SportCategory.Single);

      sport1.Length = (decimal)120;
      sport1.Width = (decimal)80;

      sport2.Length = (decimal)6;
      sport2.Width = (decimal)4;

      sport1.WriteSportProperties();
      sport2.WriteSportProperties();

      Console.Read();

    }
  }
}
