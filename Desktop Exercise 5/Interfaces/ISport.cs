﻿using Desktop_Exercise_5.Utility;

namespace Desktop_Exercise_5.Interfaces
{
  public interface ISport : IBall, ICourt
  {
    Lookups.SportCategory SportCategory { get; }
  }
}
